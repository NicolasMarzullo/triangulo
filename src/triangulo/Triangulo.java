package triangulo;

public class Triangulo {
	
	private int lado1;
	private int lado2;
	private int lado3;
	String tipo;
	
	
	public Triangulo(int lado1, int lado2, int lado3) {
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;
		this.tipo = null;
	}
	
	boolean esTrianguloValido() {
		
		if((this.lado1 < (this.lado2 + this.lado3)) && 
				(this.lado2 < (this.lado1 + this.lado3)) && 
				(this.lado3 < (this.lado1 + this.lado2))) {
			return true;
		}else {
			return false;
		}
				
	}
	
	/**
	 * La función le setea al objeto Triangulo el tipo de triangulo que es. 
	 * @return Retorna el tipo de triangulo en un string, (EQUILATERO, ISÓSCELES o ESCALENO)
	 */
	String evaluarTipoTriangulo() {
		String resultado = null;
		if(this.lado1 == this.lado2 && this.lado2 == this.lado3 && this.lado1 == this.lado3) {
			this.tipo = "EQUILATERO";
		}else if (this.lado1 != this.lado2 && this.lado2 != this.lado3 && this.lado1 != this.lado3) {
			this.tipo = "ESCALENO";
		}else {
			this.tipo = "ISÓSCELES";
		}
		return this.tipo;
	}
	
	
	

}
