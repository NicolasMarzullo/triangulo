package triangulo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Inteface extends JFrame {

	private JPanel contentPane;
	private JTextField txtLado1;
	private JTextField txtLado2;
	private JTextField txtLado3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inteface frame = new Inteface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inteface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLado = new JLabel("Lado 1");
		lblLado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLado.setBounds(114, 42, 79, 25);
		contentPane.add(lblLado);
		
		JLabel lblLado_1 = new JLabel("Lado 2");
		lblLado_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLado_1.setBounds(114, 78, 56, 35);
		contentPane.add(lblLado_1);
		
		JLabel lblLado_2 = new JLabel("Lado 3");
		lblLado_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLado_2.setBounds(114, 125, 56, 25);
		contentPane.add(lblLado_2);
		
		JLabel lblResultado = new JLabel("");
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setFont(new Font("Arial", Font.BOLD, 16));
		lblResultado.setBounds(105, 215, 226, 35);
		contentPane.add(lblResultado);
		
		txtLado1 = new JTextField();
		txtLado1.setBounds(192, 47, 86, 20);
		contentPane.add(txtLado1);
		txtLado1.setColumns(10);
		
		txtLado2 = new JTextField();
		txtLado2.setBounds(192, 86, 86, 20);
		contentPane.add(txtLado2);
		txtLado2.setColumns(10);
		
		txtLado3 = new JTextField();
		txtLado3.setBounds(190, 130, 86, 20);
		contentPane.add(txtLado3);
		txtLado3.setColumns(10);
		
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setBounds(157, 174, 95, 30);
		contentPane.add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ejecutar(txtLado1, txtLado2, txtLado3, lblResultado);			
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
	}
	
	/**
	 * Funcion que evalua los string ingresados en los TXT del front.
	 * @param lado1
	 * @param lado2
	 * @param lado3
	 * @return Devuelve si la entrada es v�lida o no
	 */
	private boolean validarEntrada(String lado1, String lado2, String lado3) {
		
		if(lado1.length() > 0 && lado2.length() > 0 && lado3.length() > 0) {
			if(Pattern.matches("^\\d{1,10}+$", lado1) && Pattern.matches("^\\d{1,10}+$", lado2) && Pattern.matches("^\\d{1,10}+$", lado3)) {
				return true;
			}else {
				System.out.println("No pas� la regex");
				return false;
			}
		}else {
			return false;
		}
		
	}
	
	
	/**
	 * Funci�n que se ejecuta cuando se aprieta el boton "calcular". La funci�n setea el correspondiente 
	 * tipo de triangulo en una etiqueta del front.
	 * @param txtLado1
	 * @param txtLado2
	 * @param txtLado3
	 * @param lblResultado
	 */
	private void ejecutar(JTextField txtLado1, JTextField txtLado2, JTextField txtLado3, JLabel lblResultado) {
		if(validarEntrada(txtLado1.getText(), txtLado2.getText(), txtLado3.getText())) {
			//Creo triangulo
			Triangulo triangulo = 
				new Triangulo
				(Integer.valueOf(txtLado1.getText()), 
						Integer.valueOf(txtLado2.getText()), Integer.valueOf(txtLado3.getText()));
			
			if(triangulo.esTrianguloValido()){
				lblResultado.setText(triangulo.evaluarTipoTriangulo());
			}else {
				lblResultado.setText("NO ES UN TRI�NGULO");
			}
		}else{
			lblResultado.setText("ERROR EN LA ENTRADA");
		}
		
	}
	
	
}
